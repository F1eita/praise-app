package com.example.praiseapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        intent = getIntent()

        val name = intent.getStringExtra(Constants.NAME)
        val surname = intent.getStringExtra(Constants.SURNAME)
        val middleName = intent.getStringExtra(Constants.MIDDLE_NAME)
        val age = intent.getStringExtra(Constants.AGE)?.toInt()
        val hobbie = intent.getStringExtra(Constants.HOBBIE)

        textSurname.setText(" Фамилия: $surname")
        textName.setText(" Имя: $name")
        textMiddleName.setText(" Отчество: $middleName")
        textAge.setText(" Возраст: $age")
        textHobbie.setText(" Хобби: $hobbie")

        var praise = ""

        when (hobbie){
            "" -> praise = " Очень грустно, что Вы ничем не увлекаетесь. " +
                        "Попробуйте что-нибудь новое или посмотрите чем увлекаются ваши друзья " +
                    "и близкие. Сходите на мастер-классы, посмотрите обучающие видео в интернете " +
                    "и Вы обязательно чему-то научитесь. Удачи в Ваших новых начинаниях!"
            else -> {
                when (age){
                    in (0..12) -> praise = " Вы так молоды и уже любите какое-то дело " +
                            "это так прекрасно, ведь впереди у Вас много лет занятия вашим " +
                            "увлечением. У Вас обязательно всё получится!"
                    in (13..60) -> praise = " Это просто потрясающе, что Вы находите время " +
                            "совмещать учёбу/работу с Вашим увлечением!!"
                    in (61..120) -> praise = " Иметь увлечение в таком возрасте достойно " +
                            "уважения. Вы, наверное, профи в своём деле!"
                    else -> praise = "Вы столько прожили!"
                }
            }
        }

        textPraise.setText(praise)
    }
}