package com.example.praiseapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            if (name.getText().toString() == "" || surname.getText().toString() == ""
                || age.getText().toString() == ""){
                textView.setText(" Поля <Имя>, <Фамилия> и <Возраст> должны быть заполнены!")
            }
            else {
                textView.setText("")

                intent = Intent(this, InfoActivity::class.java)

                intent.putExtra(Constants.NAME, name.getText().toString())
                intent.putExtra(Constants.SURNAME, surname.getText().toString())
                intent.putExtra(Constants.MIDDLE_NAME, middleName.getText().toString())
                intent.putExtra(Constants.AGE, age.getText().toString())
                intent.putExtra(Constants.HOBBIE, hobbie.getText().toString())

                startActivity(intent)
            }
        }
    }
}